# -*- coding: utf-8 -*-
import copy
import json
import scrapy

from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class ServersSpider(scrapy.Spider):
    name = 'servers'
    allowed_domains = ['cisco.com']

    headers = {
        'accept-language': 'en-US,en;q=0.9,ru;q=0.8,ru-RU;q=0.7,ja;q=0.6,pt;q=0.5',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'authority': 'ucshcltool.cloudapps.cisco.com',
        'host': 'ucshcltool.cloudapps.cisco.com',
        'origin': 'https://ucshcltool.cloudapps.cisco.com',
        'referer': 'https://ucshcltool.cloudapps.cisco.com/public/',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
    }

    headers_urlencoded = {
        'accept-language': 'en-US,en;q=0.9,ru;q=0.8,ru-RU;q=0.7,ja;q=0.6,pt;q=0.5',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'authority': 'ucshcltool.cloudapps.cisco.com',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'host': 'ucshcltool.cloudapps.cisco.com',
        'origin': 'https://ucshcltool.cloudapps.cisco.com',
        'referer': 'https://ucshcltool.cloudapps.cisco.com/public/',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
    }

    def start_requests(self):
        # loadServerTypes

        url = 'https://ucshcltool.cloudapps.cisco.com/public/rest/server/loadServerTypes'

        yield scrapy.Request(method='POST', url=url, headers=self.headers, callback=self.loadServerModels, dont_filter=True)


    def loadServerModels(self, response):
        # inspect_response(response, self)

        url = 'https://ucshcltool.cloudapps.cisco.com/public/rest/server/loadServerModels'
        data = json.loads(response.text)

        for d in data:
            search_options = {'serverType': d}
            search_options['serverType']['MANAGED'] = search_options['serverType']['MANAGED'].strip()
            meta = {'search_options': search_options}

            payload = {'treeIdRelease': d['T_ID'], 'd': d}
            body = urlencode(payload)

            yield scrapy.Request(method='POST', url=url, headers=self.headers_urlencoded, body=body, meta=meta, callback=self.loadProcessors, dont_filter=True)


    def loadProcessors(self, response):
        # inspect_response(response, self)

        url = 'https://ucshcltool.cloudapps.cisco.com/public/rest/server/loadProcessors'
        data = json.loads(response.text)

        for d in data:
            search_options = copy.copy(response.meta['search_options'])
            search_options['serverModels'] = d
            meta = {'search_options': search_options, 'd': d}

            payload = {'treeIdServerModel': d['T_ID']}
            body = urlencode(payload)

            yield scrapy.Request(method='POST', url=url, headers=self.headers_urlencoded, body=body, meta=meta, callback=self.loadOsVendors, dont_filter=True)


    def loadOsVendors(self, response):
        # inspect_response(response, self)

        url = 'https://ucshcltool.cloudapps.cisco.com/public/rest/server/loadOsVendors'
        data = json.loads(response.text)

        for d in data:
            search_options = copy.copy(response.meta['search_options'])
            search_options['processor'] = d
            meta = {'search_options': search_options, 'd': d}

            payload = {'treeIdProcessor': d['T_ID']}
            body = urlencode(payload)

            yield scrapy.Request(method='POST', url=url, headers=self.headers_urlencoded, body=body, meta=meta, callback=self.loadOsVersions, dont_filter=True)


    def loadOsVersions(self, response):
        # inspect_response(response, self)

        url = 'https://ucshcltool.cloudapps.cisco.com/public/rest/server/loadOsVersions'
        data = json.loads(response.text)

        for d in data:
            search_options = copy.copy(response.meta['search_options'])
            search_options['osVendor'] = d
            meta = {'search_options': search_options, 'd': d}

            payload = {'treeIdVendor': d['T_ID']}
            body = urlencode(payload)

            yield scrapy.Request(method='POST', url=url, headers=self.headers_urlencoded, body=body, meta=meta, callback=self.loadSearchResults, dont_filter=True)


    def loadSearchResults(self, response):
        # inspect_response(response, self)

        url = 'https://ucshcltool.cloudapps.cisco.com/public/rest/server/loadSearchResults'
        data = json.loads(response.text)

        for d in data:
            search_options = copy.copy(response.meta['search_options'])
            search_options['osVersion'] = d
            meta = {'search_options': search_options, 'd': d}

            payload = {
                'serverType_ID': search_options['serverType']['ID'],
                'serverModel_ID': search_options['serverModels']['ID'],
                'processor_ID': search_options['processor']['ID'],
                'osVendor_ID': search_options['osVendor']['ID'],
                'osVersion_ID': search_options['osVersion']['ID'],
                'firmwareVersion_ID': -1,
                'manageType': search_options['serverType']['MANAGED'],
            }
            body = urlencode(payload)

            yield scrapy.Request(method='POST', url=url, headers=self.headers_urlencoded, body=body, meta=meta, callback=self.parseSearchResults, dont_filter=True)


    def parseSearchResults(self, response):
        # inspect_response(response, self)

        data = json.loads(response.text)
        search_options = copy.copy(response.meta['search_options'])

        results = {'search_options': search_options, 'data': data}

        return results
